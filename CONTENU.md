---
    Une analyse des données provenant d'un kit "SMARTCITIZEN" (Période 2019-2021)
---

[En savoir +](INFO_PROJET.md)

---

# Contenu

## [Présentations](3_SYNTHESES/README.md)
Présentations, documents et supports de présentations

## [Synthèses](3_SYNTHESES/README.md)
Résumés, synthèses de  l'analyse de données

## [Notebooks Jupyter](2_NOTEBOOKS/README.md)
Codes en python qui ont permi de réaliser l'analyse de données, au travers de "notebooks" jupyter, via jupyterlab sous anaconda.
