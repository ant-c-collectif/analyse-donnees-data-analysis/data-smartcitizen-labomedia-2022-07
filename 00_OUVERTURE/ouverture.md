---
Descriptif : une page qui décrit l'ouverture de cette analyse de données
title: Analyse de données, ouverture
author: 
  - personnae
  - personnae
date: avril 2020
---

# Analyse de données, ouverture

## Tout commence en juillet 2022

* Début juillet 2022, je cherche des jeux de données à analyser pour m'entraîner à utiliser python pour la data-science suite à ma formation data-science python
* Je demande aux personnes membres de l'association La Labomedia si elles ont sous le coude des jeux de données que je pourrais analyser
* Fin juillet 2022, des personnes membres de l'association, me proposent des jeux de données et me les adresse par email
* Je stocke ces jeux de données sur mes dispositif de stockage numérique, dans la file de jeu de données à analyser que j'ai récupérés par ailleurs.
* Parmi ces jeux de données se trouve un jeu de données en provenance du smart-citizen kit installé dans le "fablab" de l'association.

## Ouverture de l'analyse en Décembre 2022

* Je ne commence l'analyse de ce jeu de données qu'en décembre 2022
* J'ouvre le jeu de données en décembre 2022
