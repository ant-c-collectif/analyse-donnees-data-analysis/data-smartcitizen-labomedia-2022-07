---
title: Titre
author: 
  - personnae
  - personnae
date: avril 2020
---

# Décision (go/nongo)

Au regard des informations préalables récoltées, la décision suivante est prise:

**"Go !"** pour une opération de data-science sur les données, incluant une analyse préliminaire des données et d'éventuelles analyses approfondies si nécessaire, focalisée sur les axes d'analyse suivants:

* analyse qualitative des informations de captations (dates, fréquences, ...)
* analyse des dépassements de normes de pollution
* analyse par tranche horaire de semaine 
* analyse de ressemblance des tranches horaires de semaine
* influence des openateliers les jeudi après midi
* influence des moments de confinement
* recherche de modélisatiion éventuelle et de "mise en production"