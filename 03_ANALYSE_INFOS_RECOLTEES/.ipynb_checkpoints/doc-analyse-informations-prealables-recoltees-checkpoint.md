---
descriptif: 
title: Titre
author: 
  - personnae
  - personnae
date: avril 2020
---

# Analyse des informations préalables récoltées

Les informations préalables ont été disponibles en grande majorité.

Cependant, certaines informations sont manquantes, notamment:

* les normes de pollution pour 2 mesures: ces normes sont soient absentes, soient exprimées avec une autre unité de mesure

Les informations préalables récoltées paraissent suffisantes pour entamer une opération de data-science sur les données.