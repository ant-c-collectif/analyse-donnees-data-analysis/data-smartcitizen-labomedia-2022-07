---
Descriptif:
title: Titre
author: 
  - personnae
  - personnae
date: avril 2020
---

# Démarche projet



## Itérations successives de toutes les étapes du projet

Réaliser une série d'itérations successives sur l'ensemble des étapes du processus projet (une itération débute à l'étape 1 et se termine à l'étape 9):

1. Ouverture
2. Récolte informations existantes
3. Analyse informations existantes
4. Décision: go/nogo
5. Planification
6. Réalisation des opérations de data-science (voir détails ci-après)
7. Évaluation
8. Transmission
9. Cloture

## Détails des cycles d'itération

Une série de 6 cycles d'itérations:

1. À vide
2. Avec des éléments fictifs
3. Avec quelques premiers éléments réels non approfondis, pour se rendre compte
4. Avec tous les éléments du projet
5. Revue de projet
6. Finalisation

## Détail de l'étape "Réalisation des opérations de data-science"

### Solution (Approche méthodologique retenue) 

Choix d'une approche méthodologique spécifique à la situation:

* Il s'agit de travaux libres, peronnels, sans obligation ni de contenu, ni de rendu, ni de tenue d'avancée des travaux, ni de délais.
* Choix de partage des travaux réalisés sur gitlab avec 3 personnes impliquées dans le projet  kit smart-citizen du fablab de la Labomedia.
* Choix d'une approche visant à expérimenter différentes choses avec le jeu de données fourni
* Choix d'une recherche d'un canevas méthodologique "projet" pouvant englober la démarche d'analyse de données classique et basique, et mieux l'exprimer dans une logique de projet et de prise de recul

### Démarche data-science

Choix d'une approche data-science adaptée et à la progression ajustable:

* [_] extraction
* [X] analyse préliminaire des données: pour se rendre compte 
* [X] analyses approfondies des données: pour creuser quelques aspects potentiellement intéressants
* [X] transformation (raisonnablement)
* [X] machine learning (juste quelques essais)
* [_] livraison, chargement dans un processus automatisé

### Mise en oeuvre data-science (liste d'actions)

Choix d'une série d'actions visant à coller avec ce projet de data science personnel.

**Analyses de données en python via des notebook jupyter sous anaconda**, comprenant la progression suivante:

1. chargement des données
2. analyse de la forme du jeu de données
3. analyse de fond des données
4. préparation, transformation des données
5. modélisation
6. mise en production
