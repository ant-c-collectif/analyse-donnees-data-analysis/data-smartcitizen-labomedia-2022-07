---
title: Titre
author: 
  - personnae
  - personnae
date: avril 2020
---

# Récolte d'informations préalables:

1. [ Introduction. ](#intro)
2. [ Récolte d'informations préalables sur les caractéristiques du projet. ](#caracteristiquesprojet)
3. [ Récolte d'informations préalables sur les Données. ](#donnees)
4. [ Récolte d'informations préalables sur les Livrables attendus. ](#livrables)
5. [ Récolte d'informations préalables sur les Travaux similaires déjà existants](#existant)
6. [ Informations non récoltées](#infosmanquantes)
7. [ Conclusion.](#conclusion)

<a name="intro"></a>
## Introduction

En tout premier lieu, la récolte d'informations préalables sur le projet, sur les données, et sur les éléments en lien avec ces données (pour mieux les comprendre et les expliquer, pour ajouter de l'information à ces données), va permettre de mieux cerner les dimensions du projet et de détecter les éléments qui pourraient manquer.


<a name="caracteristiquesprojet"></a>
## 1. Récolte d'informations préalables sur les caractéristiques du projet

### Résumé explicite du projet

En juillet 2022, l'association Labomedia possède dans ses locaux un dispositif smart citizen, en fonctionnement depuis 2019. Ce dispositif produit des données. Personne n'analyse ces données. L'auteur de la présente étude, membre de l'association Labomedia, recherche des jeux de données de la vie réelle, pour s'entraîner à la data-science sous python. L'association Labomedia lui délivre gracieusement un jeu de données provenant du dispositif smart-citizen de l'association. L'association n'a aucune d'exigence en termes de retours provenant de cette analyse de données. L'auteur est libre de faire toute analyse avec les données.

L'analyse de données envisagée est donc un "travail personnel libre".

### Nom du projet

"Expérimentation d'analyses de données en provenant d'un dispositif smart-citizen (2019-2022)"

### Demandeur

- Pas de demandeur "stricto-sensus" en particulier

### Destinataires

- personne en particulier
- éventuellement: 
  - les personnes adhérentes de Labomedia; le monde entier en cas de livrables accessibles en ligne publiquement.
  - les personnes souhaitant en savoir plus sur l'analyse de capteurs de pollutions de l'air dans les locaux intérieurs
  - les personnes à la recherche de compétences dans ce domaine d'analyse de données sous python

### Intentions

- De la part de Labomedia: aucune. Si des restitutions existaient ce serait chouette, mais si il n'y en a pas, ce n'est pas un problème
- De la part de Smart-Citizen: le projet Samrt-Citizen n'a pas été questionné à ce sujet.
- De la part de la personne auteur de cette analyse: 
  - s'entraîner à analyser des données réelles: analyses sous python
  - produire des contenus pouvant illustrer un savoir faire dans le cadre d'une recherche de situation professionnelle dans le domaine de la data-science sous python (data analyst python, data-science python) 

<a name="donnees"></a>
## 2. Récolte d'informations préalables sur les données

- un fichier CSV est livré. Il pèse 67.4Mo. Son ouverture rapide en tant que csv sous Jupyter se fait sans problème apparent. Il comporterait plus de 1 millions de lignes et 12 variables, soit plus de 12 millions de données. Il semble exploitable.
- le site internet smart-citizen délivre des informations techniques sur le dispositif qui fournit les données: smartcitizen kit
- sur le wiki de Labomedia, on trouve quelques informations sur le smartcitizen kit placé dans le local

<a name="livrables"></a>
## 3. Récolte d'informations préalables sur les livrables attendus

- aucun livrable n'est attendu en particulier, l'analyse de données est "une sorte d'exercice libre"
- les axes de livrables probables vers lesquels le travail s'oriente:
  - fréquences de captations
  - influence des open-ateliers du jeudi après midi
  - clustering des tranches horaires se ressemblant

<a name="existant"></a>
## 4. Récolte d'informations préalables sur des travaux similaires déjà existants

Récolte d'informations en ligne exclusivement, .

Recherches en ligne via le moteur de recherche "startpage", sur les requêtes de type:
* python timeserie air pollution air quality
* smart citizen data analysis data science
* pandas time serie
* pandas air pollution air quality
* tme serie visualization

Recherches sur le site Kaggle:
* kaggle time serie
* kaggle
* pandas
* xavier dupré
* smart citizen,
* air pollution (ou air quality),
* timeseries

### Analyses de données existantes sur les smartcitizen kit
- requête de type "smartcitizen+kit+data+analysis":
    * [informations sur l'analyse de données délivrée par smartcitizen kit](https://docs.smartcitizen.me/Data/Data%20Analysis/)
    * [analyse de données avancées provenant du projet smart citizen](https://hackmd.io/@oscgonfer/rkcvFgjxL#Advanced-data-analysis)
    * [3 exemples d'analyse de données provenant du projet smart citizen sous notebook jupyter](https://github.com/fablabbcn/smartcitizen-data/tree/master/analysis)
    * [autres liste d'exemples d'analyses de données provenant du projet smart citizen sous notebook jupyter](https://github.com/fablabbcn/smartcitizen-data/tree/master/examples)

### Analyses de données existantes sur air pollution

* les normes de pollution pour les espaces intérieurs, sont disponibles en ligne sur différents sites officiels


### Analyses de données existantes sur timeseries

Requête "kaggle time serie":

* https://www.kaggle.com/code/thebrownviking20/everything-you-can-do-with-a-time-series
* https://www.kaggle.com/code/andreshg/timeseries-analysis-a-complete-guide

Requête "pandas time serie" sur Kaggle:

* https://www.kaggle.com/code/parulpandey/getting-started-with-time-series-using-pandas 
* https://www.kaggle.com/code/bextuychiev/every-pandas-function-to-manipulate-time-series 
* https://www.kaggle.com/discussions/getting-started/125745 qui pointe ensuite vers : https://www.dataquest.io/blog/tutorial-time-series-analysis-with-pandas/
* https://www.kaggle.com/code/blackecho/pandas-time-series-analysis

- xavier dupré : http://www.xavierdupre.fr/app/ensae_teaching_cs/helpsphinx/ml2a/td2a_mlplus_timeseries_series_temporelles.html

### time serie visualisation

* https://machinelearningmastery.com/time-series-data-visualization-with-python/ 


<a name="infosmanquantes"></a>
## Informations non récoltées

Certaines informations utiles sont manquantes:

* **capteurs:** que captent-ils ? une information intantannée ? une moyenne entre deux moments ? etc ...
* **dispositif smartcitizen:** que sont les dates délivrées ? les dates de fin de captation d'un capteur (notion d'ouverture et de fermeture) ? les dates de transmission effective des mesures vers le serveur ? les dates des premiers essais de transmission des mesures vers le serveur ? les dates de réception des mesure par le serveur ?
* **normes de pollution:** certaines normes française ne sont pas bien définies au regard des mesures provenant des capteurs, il n'y a pas adéquation. Et pour l'une des mesures, elle est exprimée dans une unité de mesure qui n'est pas transposable immédiatement avec la norme française qui est exprimée dans une autre unité de mesure.

<a name="conclusion"></a>
## Conclusion

Beaucoup d'informations sont été récoltées. 

Certaines informations utiles sont toutefois manquantes.